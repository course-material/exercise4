{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Machine Learning in a Nutshell with scikit-learn\n",
    "    \n",
    "## Projections and Manifold Learning\n",
    "\n",
    "\n",
    "by \n",
    "\n",
    "[__Michael Granitzer__ (michael.granitzer@uni-passau.de)]( http://www.mendeley.com/profiles/michael-granitzer/)\n",
    "\n",
    "with examples taken from the scikit-learn documentation under http://scikit-learn.org/stable/\n",
    "\n",
    "\n",
    "__License__\n",
    "\n",
    "This work is licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Overview\n",
    "\n",
    "**Dimensionality reduction** is the mapping of data to a lower dimensional space such that uninformative variance in the data is discarded, or such that a subspace in which the data lives is detected\n",
    "\n",
    "**Two concepts are often applied**:\n",
    "\n",
    "* **Geometric Projections**: Project high-dimensional data on a low dimensional surface while preserving selected properties of the data points. The projection is global, i.e. the same for all data points\n",
    "\n",
    "    * Linear Projections\n",
    "    * Non-Linear Projections\n",
    "    \n",
    "    \n",
    "* **Manifold Learning**: Identify low-dimensional geometrical structures in high dimensional space. The projection is local, i.e. depend on the local structure of a data points.\n",
    "\n",
    "**Two main application areas**\n",
    "\n",
    "* Visualization of multivariate data sets or graphs\n",
    "* Reducing the dimensionality of a data set for increasing the accuracy and/or the efficiency of data mining algorithms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Examples \n",
    "\n",
    "The following figure shows examples for projection methods. Depending on the criteria what a *good* embedding means, different results are achieved. Top-left is the original 3D image.\n",
    "\n",
    "<img src=\"https://scikit-learn.org/stable/_images/sphx_glr_plot_compare_methods_001.png\">\n",
    "\n",
    "Note that every projection means loss of information, but sometimes it is good to loose information (e.g. noise)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Dimensionality Reduction\n",
    "\n",
    "Scikit learn supports a large number of different methods. Their usage is similar to clustering:\n",
    "\n",
    "1. Instantiate the class for doing the transformation\n",
    "2. Fit the model to the data\n",
    "3. (Optionally) apply the data \n",
    "\n",
    "We will now look into a selected set of methods"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1. PCA - Principal Component Analysis\n",
    "\n",
    "PCA decomposes a data set in its \"principal components\" through a linear transformation. Principal components are orthogonal to each other and point in the direction of largest variance, i.e. the retain the most information of the data set.\n",
    "\n",
    "Showing the direction of largest variance:\n",
    "\n",
    "<img src=\"./images/pca-variance.svg\">\n",
    "\n",
    "Transforming the coorindation system in a new one along the principal components\n",
    "\n",
    "<img src=\"./images/pca-transform.svg\">\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### The PCA Transformation\n",
    "\n",
    "Consits basically of 4 steps:\n",
    "\n",
    "<img src=\"./images/pca-princip.svg\">\n",
    "\n",
    "The dimensionality reduction happens when not all principle components are used in the new coordinate system, i.e. only those PCs with the largest variance are kept.\n",
    "\n",
    "There are nice visualizations (for 2D and 3D data) available at http://setosa.io/ev/principal-component-analysis/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### PCA in scikit-learn\n",
    "\n",
    "* Import PCA via `from sklearn.decomposition import PCA`\n",
    "* Create the instance: `pca = PCA(...parameters...)\n",
    "* Fit the data to create a model `model = pca.fit(data)` \n",
    "* transform data `transformed_data = model.transform(data)`\n",
    "* or do both in one step `transformed_data = model.fit_transform(data)`\n",
    "\n",
    "Under `model.explained_variance_ratio_` the ratio of variance explained for each principle component can be seen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Exercise1: PCA on the breast cancer data set\n",
    "\n",
    "1. Load the breast cancer data set and transform it to pandas Dataframes using the code below\n",
    "2. Have a look at the data and feature correlations to get an intuition of the dataset\n",
    "3. Conduct a PCA on the data and plot the two principle components with the highest variance (color code the classes using the labels provided in <code>y['malignant']</code>)\n",
    "4. Plot the portion of variances in the data that each component can explain (<code>pca.explained_variance_ratio_</code>). Also plot the cumulative sum of those vaues. What do these values tell us?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1. Load the breast cancer data set and transform it to pandas Dataframes using the code below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "from sklearn.datasets import load_breast_cancer\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "# necessary to not cut away columns when running out of screen\n",
    "pd.set_option('display.max_columns', None)\n",
    "\n",
    "# load the data\n",
    "dataset = load_breast_cancer()\n",
    "x = pd.DataFrame(StandardScaler().fit_transform(dataset.data), columns=dataset.feature_names)\n",
    "y = pd.DataFrame(dataset.target, columns=['malignant'])"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
